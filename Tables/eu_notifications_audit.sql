USE [SPAPPS]
GO

/****** Object:  Table [dbo].[eu_notifications_audit]    Script Date: 9/7/2018 9:48:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[eu_notifications_audit](
	[record_id] [BIGINT] IDENTITY(1,1) NOT NULL,
	[account_number] [BIGINT] NULL,
	[sent_date] [DATETIME] NULL,
 CONSTRAINT [PK_eu_notifications_audit] PRIMARY KEY CLUSTERED 
(
	[record_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[eu_notifications_audit] ADD  CONSTRAINT [DF_eu_notifications_audit_sent_date]  DEFAULT (GETDATE()) FOR [sent_date]
GO


