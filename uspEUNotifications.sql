USE SPAPPS
GO

IF EXISTS(SELECT name FROM sysobjects
        WHERE name = 'uspEUNotifications' AND type = 'P')
    DROP PROCEDURE dbo.uspEUNotifications
GO

CREATE PROCEDURE dbo.uspEUNotifications
		@Status INT OUTPUT,
		@ErrorMessage VARCHAR(MAX) OUTPUT
AS
SET NOCOUNT ON

BEGIN TRY

		DECLARE	@TableHead varchar(max)
		DECLARE @TableTail varchar(max)
		DECLARE @WebLink VARCHAR(80) 
		DECLARE @Line VARCHAR(255) = ''
		DECLARE @i INT = 1
		DECLARE @Greeting VARCHAR(255)
		DECLARE @Subject VARCHAR(MAX)
		DECLARE @AccountNumber BIGINT
		DECLARE @Body varchar(MAX)
		DECLARE @Email_To VARCHAR(MAX)
		
		IF @@SERVERNAME = 'SPDNDB'  
		BEGIN
				--production settings
				SET @WebLink = 'https://seweb.samaritan.org/AT/accounts/'
				SET @Email_To = 'BHaste@samaritan.org;CJohnston@samaritan.org'   --in production email Brittany Haste and Chris Johnston
		END
		ELSE
		BEGIN
				--test settings
				SET @WebLink = 'https://test-seweb.samaritan.org/AT/accounts/'
				SET @Email_To = 'pmills@samaritan.org;TNorris@samaritan.org'
		END
			
			


		SET @Subject = 'EU Account/Address Change Notification'

		--this table is used as the formatting print out for the emails (it builds out a string for email formatting)
		DECLARE @PrintOut TABLE
		(
			id INT IDENTITY(1, 1) PRIMARY KEY,
			Field VARCHAR(40),
			AccountInformation VARCHAR(MAX)			
		)


		--to insert account numbers in audit table
		DECLARE @Accounts_To_Audit TABLE
		(
			
			AccountNumber BIGINT		
		)

		-----------------------------------------------------------------------------------------------------------------
		--  BEGIN New EU accounts that have been created
		-----------------------------------------------------------------------------------------------------------------	
		
		DECLARE eu_addresses_cur CURSOR LOCAL STATIC FORWARD_ONLY FOR
				SELECT	am.AccountNumber 						
				FROM SPDSDEV.dbo.A01_AccountMaster am
				INNER JOIN SPDSDEV.dbo.A02_AccountAddresses aa
					ON aa.AccountNumber = am.AccountNumber
					AND aa.Active = 1
					AND ISNULL(aa.StartDate, '01/01/1980') >= DATEADD(DAY, -120, GETDATE())
				INNER JOIN SPDSDEV.dbo.A03_AddressMaster adr
					ON adr.AddressId = aa.AddressId
				INNER JOIN dbo.eu_countries ec
					ON ec.country_code = adr.Country			
				WHERE am.Status = 'A'				
				AND am.AccountNumber =  --this is not the family account so we need to compare to family account
										CASE WHEN am.FamilyId <> 0 AND am.AccountNumber <> am.FamilyId
										 THEN 
												(
													SELECT am_2.AccountNumber 
													FROM SPDSDEV.dbo.A01_AccountMaster am_2
													WHERE am_2.AccountNumber = am.FamilyId
													AND am.Status = 'A'
													GROUP BY am_2.AccountNumber
											   )									
										ELSE
										    --this is the family account number OR is not a family account
											am.AccountNumber
                                        END
				--we dont want to re-email for the same account number
				AND NOT EXISTS (SELECT ena.account_number 
								FROM dbo.eu_notifications_audit ena
								WHERE ena.account_number = am.AccountNumber)
				GROUP BY am.AccountNumber

				
				--loop cash gift documents--
				OPEN eu_addresses_cur

				FETCH NEXT FROM  eu_addresses_cur INTO @AccountNumber

				WHILE @@FETCH_STATUS = 0
				BEGIN
					  
						SET @Line = '<a href="'+ @WebLink + 
									CONVERT(VARCHAR(20), @AccountNumber) +
									'">' +
									CONVERT(VARCHAR(20), @AccountNumber) + ' ' + SPAPPS.dbo.udfFormatAccountName(@AccountNumber, 'ALL') +
									'</a>'

						
														
					    INSERT INTO @PrintOut
						( Field,  AccountInformation )
						VALUES  
						( 
								'Account: ', -- Field - varchar(40)
								@Line 
						)
						
						
						INSERT INTO @Accounts_To_Audit
						( AccountNumber)
						VALUES
						(@AccountNumber)				   
														
					
                 
                  
							FETCH NEXT FROM  eu_addresses_cur INTO @AccountNumber

				END  --end of while loop

			    CLOSE eu_addresses_cur  
			    DEALLOCATE eu_addresses_cur
		
		
		-----------------------------------------------------------------------------------------------------------------
		--  END New EU accounts that have been created
		-----------------------------------------------------------------------------------------------------------------
		
		
		-----------------------------------------------------------------------------------------------------------------------------------
		--  Format the HTML
		------------------------------------------------------------------------------------------------------------------------------------ 
		 
			SET @Greeting = 'Hi ' 
			SET @Greeting = @Greeting + ',<br><br>The following accounts either added or updated an EU address:<br><br>'
			  
			Set @TableTail = '</table></body></html>';
			Set @TableHead = '<html><head>' +
							  '<style>' +
							  'td {border: solid black 1px;padding-left:5px;padding-right:5px;padding-top:1px;padding-bottom:1px;font-size:11pt;} ' +
							  '</style>' +
							  '</head>' +
							   @Greeting +
							  '<body><table cellpadding=0 cellspacing=0 border=0>' +
							  '<tr bgcolor=#FFEFD8><td style="align=center;border-right:0px solid"><b></b></td>' +
							  '<td align=center><b>Account Information</b></td></tr>';
		 
			SELECT @Body = (SELECT field AS [TD], AccountInformation AS [TD] FROM @PrintOut ORDER BY id
			FOR XML raw('tr'), ELEMENTS)
		 
			-- Replace the entity codes and row numbers
			SET @Body = REPLACE(@Body, '_x0020_', SPACE(1))
			SET @Body = REPLACE(@Body, '_x003D_', '=')
			SET @Body = REPLACE(@Body, '<tr><TRRow>1</TRRow>', '<tr bgcolor=#C6CFFF>')
			SET @Body = REPLACE(@Body, '<TRRow>0</TRRow>', '')
			SET @Body = REPLACE(@Body, '&lt;', '<')
			SET @Body = REPLACE(@Body, '&gt;', '>')
		 
			SET @Body = @TableHead + @Body + @TableTail

		-----------------------------------------------------------------------------------------------------------------------------------
		--  END Format the HTML
		------------------------------------------------------------------------------------------------------------------------------------

		-----------------------------------------------------------------------------------------------------------------------------------
		-- BEGIN Send Email
		------------------------------------------------------------------------------------------------------------------------------------
		--send the email (if there are records in the account table)
		IF EXISTS (SELECT 1 
			FROM @Accounts_To_Audit 
			)
		EXEC MSDB.DBO.SP_SEND_DBMAIL 
			@recipients = @Email_To,
			@subject = @Subject,
			@body = @Body,
			@body_format = 'HTML'

			
			INSERT INTO dbo.eu_notifications_audit
			(
			    account_number
			)			
			SELECT AccountNumber
			FROM @Accounts_To_Audit
			EXCEPT
            SELECT na.account_number 
			FROM dbo.eu_notifications_audit na
			
			
		-----------------------------------------------------------------------------------------------------------------------------------
		-- END Send Email
		------------------------------------------------------------------------------------------------------------------------------------
			
			
 

END TRY
BEGIN CATCH
	SELECT @Status = 1 
	SELECT @ErrorMessage = SUBSTRING('ERROR IN  = uspEUNotifications, PROCEDURE LINE ' + 
								   RTRIM(CONVERT(CHAR(10), ERROR_LINE())) + ', ' +
								   ERROR_MESSAGE(), 1, 255) 
	IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
END CATCH
GO


		